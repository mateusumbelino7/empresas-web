import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import logo from "./logo.png";
import "./style.css";

import empresasService from "../../services/empresas";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();
  const { setToken } = useContext(empresasService.authContext);

  const login = async () => {
    const result = await empresasService.login(email, password, setToken);
    if (result) {
      history.push("/");
    }
  };

  return (
    <section className="Login">
      <img src={logo} alt="ioasys logo" />
      <h1>bem-vindo ao empresas</h1>
      <form>
        <div className="input-field">
          <i className="material-icons prefix">email</i>
          <input
            id="email"
            type="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
          <label htmlFor="email">Email</label>
        </div>
        <div className="input-field">
          <i className="material-icons prefix">lock_open</i>
          <input
            id="password"
            type="password"
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
          <label htmlFor="password">Password</label>
        </div>
        <button type="button" className="darken-1 waves-effect waves-light btn" onClick={login}>
          entrar
        </button>
      </form>
    </section>
  );
}

export default Login;
