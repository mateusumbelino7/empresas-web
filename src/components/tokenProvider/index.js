import React, { useState } from 'react'

import empresasService from '../../services/empresas'

function TokenProvider({ children }) {
    const [token, setToken] = useState(null)

    return (
        <empresasService.authContext.Provider value={{token, setToken}}>
            {children}
        </empresasService.authContext.Provider>
    )
}

export default TokenProvider
