import React, { useState, useEffect, useContext } from 'react'
import { useParams } from "react-router-dom"

import empresaService from '../../services'

function Empresa() {
    const [empresa, setEmpresa] = useState(null)
    const { id } = useParams()
    const {token} = useContext(empresaService.authContext)

    useEffect(() => {
        setEmpresa(await empresaService.getEmpresa(id, token))
    }, [])

    if (empresa === null) return 'loading'
    return (
        <section>
            <img src={empresa.image} />
            <p>{empresa.description}</p>
        </section>
    )
}

export default Empresa