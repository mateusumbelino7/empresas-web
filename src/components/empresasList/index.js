import React, { useState, useEffect, useContext } from "react";

import empresasService from "../../services/empresas";

function Empresa({ name, type, location, image }) {
  return <div>{name}</div>;
}

function EmpresasList() {
  const [empresas, setEmpresas] = useState([]);
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const { token } = useContext(empresasService.authContext);

  useEffect(async () => {
    setEmpresas(await empresasService.getListaEmpresas(type, name, token));
  }, [type, name]);

  return (
    <section>
      <div>
        <input
          type="text"
          value={name}
          onInput={e => setName(e.target.value)}
        />
        <input
          type="text"
          value={type}
          onInput={e => setType(e.target.value)}
        />
      </div>
      <div>{empresas.map(Empresa)}</div>
    </section>
  );
}

export default EmpresasList;
