import { createContext } from "react";

const authContext = createContext();

const base = "http://empresas.ioasys.com.br/api/v1";

async function login(email, password, setToken) {
  const response = await fetch(`${base}/users/auth/sign_in`, {
    method: "POST",
    body: JSON.stringify({
      email,
      password
    }),
    headers: {
      "Content-Type": "application/json"
    }
  });
  const { "access-token": accessToken, client, uid } = response.headers;
  setToken({ accessToken, client, uid });
}

async function getListaEmpresas(type, name, token) {
  const query = {};
  if (type !== "") query.enterprise_types = type;
  if (name !== "") query.name = name;
  const url = new URL(`${base}/enterprises`);
  url.search = new URLSearchParams(query);

  const response = await fetch(url, {
    headers: {
      "access-token": token["access-token"],
      client: token.client,
      uid: token.uid
    }
  });
  return await response.json();
}

async function getEmpresa(id, token) {
  const response = await fetch(`${base}/enterprises/${id}`, {
    headers: {
      "access-token": token["access-token"],
      client: token.client,
      uid: token.uid
    }
  });
  return await response.json();
}

export default {
  login,
  getListaEmpresas,
  getEmpresa,
  authContext
};
