import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Login from "./components/login";
import TokenProvider from "./components/tokenProvider";

function App() {
  return (
    <TokenProvider>
      <Router>
        <Switch>
          <Route path="/">
            <Login />
          </Route>
        </Switch>
      </Router>
    </TokenProvider>
  );
}

export default App;
